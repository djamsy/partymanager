// =============================================================================
// Dependencies.
// =============================================================================

// Node modules.
const async = require('async');
const path = require('path');
const pdf = require('html-pdf');
const fs = require('fs');
const qr = require('qr-image');

// Own modules.
const router = require(path.join(__dirname, '..', 'router', 'index'));
const modules = require(path.join(__dirname, '..', 'modules', 'index'));
const queries = require(path.join(__dirname, '..', 'tools', 'queries'));

// =============================================================================
// Arguments.
// =============================================================================

// Ragenerate codes. Enabled by passing argument '--regenerate'.
var regenerate = (process.argv.length >2 && process.argv[2] === '--regenerate');

// =============================================================================
// Constants.
// =============================================================================

// QR code target directory.
const qrHome = path.join(__dirname, '..', 'files', 'images', 'qr');

// Source HTML file name.
const htmlTemplate = path.join(__dirname, '..', 'public', 'html', 'flyer.html');

// Target PDF file name.
const pdfHome = path.join(__dirname, '..', 'files', 'docs', 'invitations');

// QR generation options.
const qrOptions = {
  type: 'png',
  margin: 1,
  size: 12
};

// Conversion options.
const pdfOptions = {
  base: 'file://',
  quality: "100",
  height: "915px",
  width: "708px",
	zoomFactor: 1,
  border: {
    top: "10px",
    right: "10px",
    bottom: "10px",
    left: "10px"
  },
};

// =============================================================================
// Main.
// =============================================================================

// Load modules.
modules.load(function(error) {
  if(error) {
		console.log(error);
		process.exit(1);
	}

  // Get query depending on '--regenerate' option.
  var query = queries.users.getWithoutCode;
  if(regenerate) {
    query = queries.users.getAll;
  }

  // Search for all users that don't have a code.
  Query(query, [], function(error, results) {
    if(error) {
      log.error(error);
      process.exit(1);
    }
    if(!results.length) {
      log.info('No results found.');
      process.exit(0);
    }
    // Get users.
    var users = results;
    // Initialize counter.
    var count = 0;
    log.info('Users to update: %d.', users.length);
    // Generate code for each user.
    async.eachSeries(users, function(user, userCb) {
      var newCode = fns.generateCode();
      Query(queries.users.updateCode, [newCode, user.id], function(error) {
        if(error) {
          return userCb(error);
        }
        count++;
        log.info('Generated code %d/%d (%d%%): %s for user %s.', count, users.length, Math.round(count/users.length*10000)/100, newCode, user.name);
        // Generate QR code.
        var qrFile = path.join(qrHome, newCode +'.png');
        var qrImage = qr.imageSync(newCode, qrOptions);
        // Save to file.
        fs.writeFile(qrFile, qrImage, function(error) {
          if(error) {
            return userCb(error);
          }
          log.info('Generated QR code for user %s: %s', user.name, qrFile);
          // Load HTML file.
          var html = fs.readFileSync(htmlTemplate, 'utf8');
          // Replace variables into HTML template.
          html = fns.replaceVars(html, {code: newCode});
          // Build target file name.
          var pdfFile = path.join(pdfHome, user.name +'.pdf');
          pdf.create(html, pdfOptions).toFile(pdfFile, function(error, res) {
        		if(error) {
        			log.error(error);
        			return process.exit(1);
        		}
        		log.info('Generated invitation for user %s: %s.', user.name, res.filename);
            userCb();
        	});
        });
      });
    }, function(error) {
      if(error) {
        log.error(error);
        process.exit(1);
      }
      log.info('Finished generating %d codes.', users.length);
      process.exit(0);
    });
  });
});
