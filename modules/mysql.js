// =============================================================================
// Dependencies
// =============================================================================

// Node modules
const path = require('path');
const mysql = require('mysql');

// Own modules
const fns = require(path.join(__dirname, '..', 'tools', 'functions'));

// =============================================================================
// Module exports
// =============================================================================

// Export order
module.exports.order = 3;

// Load
// Parameters:
// - cb:   [Function] Callback.
exports.load = function(cb) {
	// Create connection
	var connection = mysql.createConnection({
		host     : config.mysqlHost,
		user     : config.mysqlUser,
		password : config.mysqlPass,
		database : config.mysqlSchema
	});

	// SQL injection fixer
	global.sanitizeSQL = function(value) {
		if(!value || !value.length) {
			return '';
		}
		var escapedValue = mysql
			.escape(value)
			// Comillas al inicio de la consulta
			.replace(/^\'/g, '')
			// Carácter '?' en la consulta
			.replace(/\?/gi, '')
			// Comillas al final de la consulta
			.replace(/\'$/g, '');
		// Also, remove JS
		return removeJS(escapedValue);
	};

	// Remove HTML <script> tags and everything inside them
	global.removeJS = function(value) {
		if(!value || !value.length) {
			return '';
		}
		return value.replace(/<[\s]*script[\s]*\b[^<]*(?:(?!<[\s]*\/[\s]*script[\s]*>[\s]*)<[^<]*)*<[\s]*\/[\s]*script[\s]*>/g, '');
	};

	// Remove all HTML tags and everything inside them
	global.removeHTML = function(value) {
		if(!value || !value.length) {
			return '';
		}
		// First, remove JS
		var escapedValue = removeJS(value);
		// Then, remove HTML
		return escapedValue.replace(/<(?:.|\n)*?>/gm, '');
	};

	// Remove conflictive HTML and leave just bsaic tags
	global.removeConflictiveHTML = function(value) {
		if(!value || !value.length) {
			return '';
		}
		// First, remove JS
		var escapedValue = removeJS(value);
		// Then, leave only allowed HTML tags
		return escapedValue.replace(new RegExp('\<[\s]*(?![\s]*'+ config.nonConflictiveHtmlTags.join('|[\s]*') +').*?\>', 'g'), '');
	};

	// Function for performing queries
	global.Query = function (sql, params, queryCb) {
		// Enable single param substitution
		if(typeof(params) === 'string') {
			params = [params];
		}
		// Remove JS from params
		for(var i in params) {
			if(typeof(params[i]) === 'string') {
				params[i] = removeJS(params[i]);
			}
		}
		// Perform query
		var query = connection.query(sql, params, queryCb);
		log.query(query.sql);
	};
	cb();
};
