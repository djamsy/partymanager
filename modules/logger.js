// =============================================================================
// Requirements.
// =============================================================================

// Node modules.
const _ = require('underscore');
const path = require('path');
const colors = require('colors');

// Own modules.
const fns = require(path.join(__dirname, '..', 'tools', 'functions'));

// =============================================================================
// Generic functions.
// =============================================================================

// Logger function.
const log = function(type, message) {
	var d = fns.loggerDateTime();
	console.log.apply(this, ['['+ d +'] [' + type + '] '+ _.first(message)].concat(_.rest(message)));
};

// =============================================================================
// Module exports.
// =============================================================================

// Export order.
module.exports.order = 2;

// Load.
// Parameters:
// - cb:   [Function] Callback.
exports.load = function(cb) {
	// Export global logging function.
	global.log = {
		error: function() {
			if(config.logLevel >= 1) {
				return log('error'.red, arguments);
			}
		},
		warn: function() {
			if(config.logLevel >= 2) {
				return log('warn'.green, arguments);
			}
		},
		info: function() {
			if(config.logLevel >= 3) {
				return log('info'.blue, arguments);
			}
		},
		query: function() {
			// Same log level as 'info'.
			if(config.logLevel >= 3) {
				return log('query'.yellow, arguments);
			}
		},
		debug: function() {
			if(config.logLevel >= 4) {
				return log('debug'.grey, arguments);
			}
		}
	};
	cb();
};
