// =============================================================================
// Dependencies.
// =============================================================================

//	Node modules.
const glob = require('glob');
const async = require('async');
const path = require('path');

// Own modules.
const fns = require(path.join(__dirname, '..', 'tools', 'functions'));

// =============================================================================
// Module exports.
// =============================================================================

module.exports = {
	load : function(cb) {
		// Find files in same folder.
		const files = glob.sync(path.join(__dirname, '*.js'));
		// Initialize ordered module list.
		var orderedModules = {};
		// Create list of ordered files to import.
		files.forEach(function(file) {
			orderedModules[require(file).order] = file;
		});
		orderedModules = fns.orderByKey(orderedModules);
		// Require them in order.
		async.eachSeries(orderedModules, function(file, moduleCb) {
			if (/index.js$/.test(file)) {
				return moduleCb();
			}
			require(file).load(moduleCb);
		}, cb);
	}
};
