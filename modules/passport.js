// =============================================================================
// Dependencies.
// =============================================================================

// Node modules.
const path = require('path');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

// Own modules.
const auth = require(path.join(__dirname, '..', 'controllers', 'auth'));

// =============================================================================
// Module exports.
// =============================================================================

// Export order.
module.exports.order = 4;

// Load.
// Parameters:
// - cb:   [Function] Callback.
exports.load = function(cb) {
	// Serializing functions
	passport.serializeUser(auth.serializeUser);
	passport.deserializeUser(auth.deserializeUser);

	// Authentication strategy.
	var localStrategy = new LocalStrategy({
		usernameField: 'username',
		passwordField: 'password'
	}, auth.localAuth);

	// Make Passport use strategy.
	passport.use(localStrategy);
  cb();
};
