// =============================================================================
// Dependencies.
// =============================================================================

// Node modules.
const _ = require('underscore');
const fs = require('fs');
const path = require('path');
const colors = require('colors');

// Own modules.
const fns = require(path.join(__dirname, '..', 'tools', 'functions'));
const validate = require(path.join(__dirname, '..', 'tools', 'validator'));
const defaultConfig = require(path.join(__dirname, '..', 'conf', 'config.dist'));

// =============================================================================
// Parameters.
// =============================================================================

// Package configuration (from package.json).
var packageConfig = {};
// Extra configuration properties.
var extraConfig = {};
// Party configuration properties.
var partyConfig = {};

const loadExtraConfiguration = function(customConfig) {
	// Package config (from package.json).
	packageConfig = JSON.parse(fs.readFileSync(path.join(__dirname, '..', 'package.json'), 'utf8'));

	// Extra configuration properties.
	extraConfig.faviconLocation = 'public/favicon/favicon.ico';
	extraConfig.logo = fns.getLogoASCII().white;
	extraConfig.baseDomain = customConfig.domain + (customConfig.domain === 'localhost' ? ':' + customConfig.port : '');
	extraConfig.baseUrl = 'http://' + extraConfig.baseDomain;
	extraConfig.maxUploadFileSize = 4194304 /* 4 MB (in Bytes) */;
	extraConfig.nonConflictiveHtmlTags = ['i', '\/i', 'b', '\/b', 'br', 'p', '\/p', 'img', 'h1', '\/h1', 'h2', '\/h2',
		'h3', '\/h3', 'h4', '\/h4', 'h5', '\/h5', 'h6', '\/h6', 'ul', '\/ul', 'ol', '\/ol', 'li', '\/li', 'u', '\/u',
		'small', '\/small', 'blockquote', '\/blockquote', 'a', '\/a'];
	extraConfig.userImageLocation = '/images/';
	extraConfig.guestCodeLength = 4;
	extraConfig.introMsg = "" +
		(packageConfig.name ?     "Project: " + packageConfig.name.bgBlack +     ".\n" : "") +
		(packageConfig.version ?  "Version: " + packageConfig.version.bgBlack +  ".\n" : "") +
		(packageConfig.homepage ? "Docs:    " + packageConfig.homepage.bgBlack + ".\n" : "");

	// Party configuration.
	partyConfig.title = customConfig.partyTitle;
	partyConfig.date = customConfig.partyDate;
	partyConfig.description = customConfig.partyDescription;
};

// =============================================================================
// Module exports.
// =============================================================================

// Export order.
module.exports.order = 1;

// Load.
// Parameters:
// - cb:   [Function] Callback.
module.exports.load = function(cb) {

	// Check that configuration file exists.
	if(!fs.existsSync(path.join(__dirname, '..', 'conf', 'config.js'))) {
		console.log('File "config.js" not found in ' + path.join(__dirname, '..', 'conf'));
		console.log('');
		process.exit(1);
	}

	// Validate config object.
	var customConfig = validate(require('../conf/config'));

	// Load extra configuration.
	loadExtraConfiguration(customConfig);

	// Append package.json properties and extra config properties to config.
	customConfig = _.extend(customConfig, extraConfig);
	customConfig = _.extend(customConfig, {party:   partyConfig});
	customConfig = _.extend(customConfig, {project:   packageConfig});

	// Return config object.
	global.config = customConfig;

	// Callback.
	cb();
};
