// =============================================================================
// Dependencies.
// =============================================================================

// Node modules.
const _ = require('underscore');
const glob = require('glob');
const path = require('path');
const async = require('async');

// Own modules (export them as global objects).
global.fns = require(path.join(__dirname, '..', 'tools', 'functions'));
global.crypt = require(path.join(__dirname, '..', 'tools', 'crypt'));
global.errors = require(path.join(__dirname, '..', 'tools', 'errors'));
global.queries = require(path.join(__dirname, '..', 'tools', 'queries'));
global.messages = require(path.join(__dirname, '..', 'tools', 'messages'));

// =============================================================================
// Aux functions.
// =============================================================================

// Function to render an error page.
const renderError = function(req, res) {
	return function(errorCode, trace, layout) {
		// Get layout.
		layout = layout || 'layout';
		// Prerender.
		module.exports.prerender(req, res);
		// Build error object.
		res.locals.error = {
			code:    errors[errorCode].code,
			status:  errors[errorCode].status,
			message: (trace ? (trace.message || trace) : errors[errorCode].message),
			image:   errors[errorCode].image
		};
		// Render page.
		log.error(res.locals.error.message);
		log.debug('Rendering error page "%d."', errorCode);
		return res.render('error', {layout: layout});
	};
};

// =============================================================================
// Module exports.
// =============================================================================

// Init.
module.exports.init = function(req, res, next) {
	// Append renderError function to 'res' object.
	res.renderError = renderError(req, res);
	next();
};

// Render common locals.
module.exports.prerender = function(req, res) {
	res.locals._ =           _;
	res.locals.baseUrl =     config.baseUrl;
	res.locals.defaultLang = config.lang;
	res.locals.fns =         fns;
	res.locals.error =       res.locals.error || req.error || null;
	res.locals.flash =       res.locals.flash || req.flash || null;
	res.locals.partyConfig = config.party;
	res.locals.path =        req.path;
	res.locals.queryParams = req.query;
	res.locals.user =        req.user || null;
	res.locals.version =     config.project.version;
	res.locals.year =        (new Date()).getFullYear();
};

// Function to render a page.
module.exports.render = function(page, layout) {
	// Default layout.
	layout = layout || 'layout';
	return function(req, res) {
		// Prerender.
		module.exports.prerender(req, res);
		// Render page.
		log.debug('Rendering page: "%s" with layout: "%s".', page, layout);
		res.render(page, {layout: layout});
	};
};

// Function to redirect.
module.exports.redirect = function(url) {
	return function(req, res) {
		return res.redirect(url || req.query.redir || '/');
	};
};

// Default fallback.
module.exports.fallback = function(req, res, next) {
    log.debug('Unknown endpoint: "%s".', req.path);
    return renderError(req, res)(404);
};

// =============================================================================
// Export other controllers.
// =============================================================================

// Export JS files in same folder as part of controllers.
var files = glob.sync(path.join(__dirname, '*.js'));
files.forEach(function(file) {
	var name = path.basename(file, '.js');
	if(name !== 'index') {
		module.exports[name] = require(file);
    }
});
