// =============================================================================
// Dependencies.
// =============================================================================

// Node modules.
const path = require('path');
const async = require('async');
const passport = require('passport');

// Own modules.
const controllers = require(path.join(__dirname, '..', 'controllers'));

// =============================================================================
// Authentication requirements.
// =============================================================================

module.exports.require = {
    // Require an user.
    user: function(req, res, next) {
        if(!req.user) {
            log.debug('No user');
            return res.redirect('/login?redir=' + req.path);
        }
        next();
    }
};

// =============================================================================
// Aux functions.
// =============================================================================

// Authentication callback.
const authCallback = function(req, res, next) {
    return function(error, user) {
        if (error) {
            req.error = error;
            return next();
        }
        if (!user) {
            req.error = errors.DENIED_ACCESS;
            return next();
        }
        req.logIn(user, function(error) {
            if (error) {
                req.error = errors.LOGIN_ERROR;
                return next();
            }
            log.info('User login: ID: %d, username: %s.', req.user.id, req.user.username);
            // Perform actions on database on user login
            Query(queries.users.updateLoginInfo, [new Date(), req.ip, user.id], function(error) {
              if(error) {
                  req.error = error;
              }
              return next();
            });
        });
    };
};

// =============================================================================
// Module exports.
// =============================================================================

// Function for logging in.
module.exports.login = function(req, res, next) {
    passport.authenticate('local', authCallback(req,res, next))(req, res, next);
};

// Log out.
module.exports.logout = function(req, res, next) {
    req.logout();
    next();
};

// Auth strategy, coming from passport.js.
module.exports.localAuth = function(username, password, done) {
    // Find user by username or username.
    Query(queries.users.getByUsername, username, function(error, results) {
        if(error) {
            return done(error);
        }
        // Save found user.
        var user = results[0];
        // Not found.
        if(!user) {
          return done(errors.INVALID_USER);
        }
        // Incorrect password.
        if(user.password !== crypt(password)) {
          return done(errors.WRONG_PASSWORD);
        }
        // Too early to log in.
        if(user.login_from && (new Date()) < (new Date(user.login_from))) {
          return done(fns.formatError(errors.EARLY_LOGIN, [user.login_from]));
        }
        // Too late to log in.
        if(user.login_to && (new Date()) > (new Date(user.login_to))) {
          return done(fns.formatError(errors.LATE_LOGIN, [user.login_to]));
        }
        return done(null, user);
    });
};

// Serialize user.
module.exports.serializeUser = function(user, done) {
    log.debug('Serializing user: ID: %d', user.id);
    done(null, user.id);
};

// Deserialize user.
module.exports.deserializeUser = function(id, done) {
    log.debug('Deserializing user: ID: %d', id);
    // Perform query.
    controllers.users.getUser(id, function(error, user) {
      if(error) {
        return done(error);
      }
      return done(null, user);
    });
};
