// =============================================================================
// Dependencies.
// =============================================================================

// Node modules.
const _ = require('underscore');
const fs = require('fs');
const path = require('path');
const async = require('async');

// Own modules.
const controllers = require(path.join(__dirname, '..', 'controllers'));
const model = require(path.join(__dirname, '..', 'model', 'db'));

// =============================================================================
// Module exports.
// =============================================================================

/**
 * Singleton to get user data.
 * Parameters:
 * - id: [Number]   ID of the user.
 * - cb: [Function] Callback function, containing parameters 'error' and 'user'.
**/
module.exports.getUser = function(id, cb) {
	Query(queries.users.getById, id, function(error, results) {
		if(error) {
			return cb(error);
		}
		if (!results.length) {
			return cb(fns.formatError(errors.NONEXISTING_USER, id));
		}
		var user = results[0];
		user.profile_picture = fns.getProfilePicture(user);
		cb(null, user);
	});
};

/**
 * Error callback for a req, resfunction.
**/
const errorCb = function(req, next, error) {
	if(typeof(error) === 'string') {
		log.error(error);
	} else if(typeof(error) === 'object') {
		if(error.message) {
			log.error(error.message);
		} else {
			log.error(error +'');
		}
	}
	req.error = error;
	return next();
};

/**
 * Register the entry of a user.
**/
module.exports.enter = function(req, res, next) {
	if(!req.body.code) {
		return errorCb(req, next, fns.formatError(errors.REQUIRED_FIELD, ['code']));
	}
	if(!fns.validateCode(req.body.code)) {
		return errorCb(req, next, fns.formatError(errors.WRONG_CODE, [req.body.code]));
	}
	// Get user with that code.
	Query(queries.users.getByCode, req.body.code, function(error, results) {
		if(error) {
			return errorCb(req, next, error);
		}
		if(!results.length) {
			return errorCb(req, next, fns.formatError(errors.WRONG_CODE, [req.body.code]));
		}
		var user = results[0];
		if(+user.has_entered === 1) {
			return errorCb(req, next, fns.formatError(errors.ALREADY_ENTERED, [user.name, req.body.code, fns.beautifyTime(user.last_entry), user.registerer]));
		}
		var date = new Date();
		// Register the movement.
		Query(queries.movements.new, [user.id, req.user.id, date, 'entry'], function(error) {
			if(error) {
				return errorCb(req, next, error);
			}
			// Update status of the user.
			Query(queries.users.userEntry, [date, user.id], function(error) {
				if(error) {
					return errorCb(req, next, error);
				}
				user.has_entered = 1;
				user.last_entry = date;
				user.profile_picture = fns.getProfilePicture(user);
				req.data = user;
				return next();
			});
		});
	});
};

/**
 * Register the exit of a user.
**/
module.exports.exit = function(req, res, next) {
	if(!req.body.code) {
		return errorCb(req, next, fns.formatError(errors.REQUIRED_FIELD, ['code']));
	}
	if(!fns.validateCode(req.body.code)) {
		return errorCb(req, next, fns.formatError(errors.WRONG_CODE, [req.body.code]));
	}
	// Get user with that code.
	Query(queries.users.getByCode, req.body.code, function(error, results) {
		if(error) {
			return errorCb(req, next, error);
		}
		if(!results.length) {
			return errorCb(req, next, fns.formatError(errors.WRONG_CODE, [req.body.code]));
		}
		var user = results[0];
		if(+user.has_entered === 0) {
			return errorCb(req, next, fns.formatError(errors.ALREADY_EXITED, [user.name, req.body.code, fns.beautifyTime(user.last_exit), user.registerer]));
		}
		var date = new Date();
		// Register the movement.
		Query(queries.movements.new, [user.id, req.user.id, date, 'exit'], function(error) {
			if(error) {
				return errorCb(req, next, error);
			}
			// Update status of the user.
			Query(queries.users.userExit, [date, user.id], function(error) {
				if(error) {
					return errorCb(req, next, error);
				}
				user.has_entered = 0;
				user.last_exit = date;
				user.profile_picture = fns.getProfilePicture(user);
				req.data = user;
				return next();
			});
		});
	});
};

/**
 * Get the registry of entries/exits of users.
**/
module.exports.movements = function(req, res, next) {
	Query(queries.movements.getAll, [], function(error, results) {
		if(error) {
			return errorCb(req, next, error);
		}
		req.data = results;
		return next();
	});
};

/**
 * Get the list of users.
**/
module.exports.list = function(req, res, next) {
	Query(queries.users.getAll, [], function(error, results) {
		if(error) {
			return errorCb(req, next, error);
		}
		req.data = results;
		return next();
	});
};
