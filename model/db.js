// =============================================================================
// Dependencies
// =============================================================================

// Node modules.
const path = require('path');
const nodeddl = require('nodeddl');

// Module properties.
const types = nodeddl.types;
const options = nodeddl.options;

// Own modules.
const config = require(path.join(__dirname, '..', 'conf', 'config.js'));

// =============================================================================
// Module exports.
// =============================================================================

// Export.
module.exports = {
	database: {
		name:      config.mysqlSchema,
		collation: 'utf8_general_ci'
	},
	timezone:  'SYSTEM',
	user: {
		name:     config.mysqlUser,
		password: config.mysqlPass,
		host:     config.mysqlHost
	},
	tables: {
		users: {
			columns: {
				'id': {
					type: types.int(11),
					options: [options.not_null(), options.auto_increment()]
				},
				'username': {
					type: types.varchar(255),
					options: [options.default(null), options.comment('Username')]
				},
				'password': {
					type: types.varchar(255),
					options: [options.default(null), options.comment('User password')]
				},
				'name': {
					type: types.varchar(63),
					options: [options.default(null), options.comment('User first name')]
				},
				'role': {
					type: types.enum('host', 'staff', 'guest', 'artist'),
					options: [options.default('guest'), options.comment('User role')]
				},
				'code': {
					type: types.varchar(8),
					options: [options.default(null), options.comment('Access token')]
				},
				'has_entered': {
					type: types.int(1),
					options: [options.default(0), options.comment('Whether the user is in the party or not')]
				},
				'paid': {
					type: types.int(1),
					options: [options.default(0), options.comment('Whether the user has paid for the party or not')]
				},
				'date_created': {
					type: types.datetime(),
					options: [options.default('CURRENT_TIMESTAMP'), options.comment('Creation date')]
				},
				'last_entry': {
					type: types.datetime(),
					options: [options.default(null), options.comment('Last entry date')]
				},
				'last_exit': {
					type: types.datetime(),
					options: [options.default(null), options.comment('Last exit date')]
				},
				'last_ip': {
					type: types.varchar(63),
					options: [options.default(null), options.comment('Last login IP')]
				},
				'last_login': {
					type: types.datetime(),
					options: [options.default(null), options.comment('Last login date')]
				},
				'login_from': {
					type: types.datetime(),
					options: [options.default(null), options.comment('Min. date from which a user can log in')]
				},
				'login_to': {
					type: types.datetime(),
					options: [options.default(null), options.comment('Max. date from which a user can log in')]
				}
			},
			primary_key: options.primary_key('id')
		},
		movements: {
			columns: {
				'id': {
					type: types.int(11),
					options: [options.not_null(), options.auto_increment()]
				},
				'id_user': {
					type: types.int(11),
					options: [options.default(null), options.comment('ID of the user that performed the movement')]
				},
				'id_registerer': {
					type: types.int(11),
					options: [options.default(null), options.comment('ID of the user that registered the movement')]
				},
				'date': {
					type: types.datetime(),
					options: [options.default('CURRENT_TIMESTAMP'), options.comment('Movement date')]
				},
				'type': {
					type: types.enum('entry', 'exit'),
					options: [options.default(null), options.comment('Type of the movement')]
				}
			},
			foreign_keys: [
				options.foreign_key('id_user', 'id', 'users'),
				options.foreign_key('id_registerer', 'id', 'users')
			],
			primary_key: options.primary_key('id')
		}
	}
};
