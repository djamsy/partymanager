// Stream variable.
var track;

// Show an error on index page.
const showIndexError = function(msg) {
  $('#index-error').html(msg);
  $('.alert-danger').show();
};

// Hide error on index page.
const hideIndexError = function() {
  $('#index-error').html('');
  $('.alert-danger').hide();
};

// Hide success message.
const hideIndexSuccess =function() {
  $('.alert-success').hide();
};

// Show the success message.
const showIndexSuccess = function(user, type) {
  var msg = '';
  switch(type) {
    case 'enter':
      msg = 'Entrada registrada a las '+ beautifyTime(user.last_entry) +'.';
      break;
    case 'exit':
      msg = 'Salida registrada a las '+ beautifyTime(user.last_exit) +'.';
      break;
  }
  $('.alert-success img').attr('src', user.profile_picture);
  $('.alert-success .user-name').html(user.name);
  $('.alert-success .user-role span').html(user.role);
  $('.alert-success .user-role span').removeClass();
  $('.alert-success .user-role span').addClass('role-'+ user.role);
  $('.alert-success .alert-msg').html(msg);
  $('.alert-success').show();
};

// Register the status of a user.
const registerUserStatus = function(code, type) {
  // Reset error and success messages.
  hideIndexError();
  hideIndexSuccess();
  // Validate code (just the length; the rest will be done server-side).
  if(!code|| !code.length) {
    return showIndexError('Por favor, escribe un código.');
  }
  // Perform POST.
  $.post('/api/user/'+ type, {code: code.toUpperCase()})
  .done(function(data) {
    if(data.error) {
      return showIndexError(data.error);
    }
    showIndexSuccess(data.data, type);
  })
  .fail(function(jqXHR, textStatus, errorThrown) {
    showIndexError(textStatus +': '+ errorThrown +'\n'+ jqXHR);
  });
};

// Register the entry of a user.
const userEntry = function(code) {
  return registerUserStatus(code, 'enter');
};

// Register the exit of a user.
const userExit = function(code) {
  return registerUserStatus(code, 'exit');
};

// Register the consumption of a drink by a user.
const userDrink = function(code) {
  // TODO.
};

// Get the status of users.
const getGuestStatus = function(cb) {
  $.get('/api/users')
  .done(function(data) {
    if(data.error) {
      console.log(data.error);
      return cb(null);
    }
    cb(data.data);
  })
  .fail(function(jqXHR, textStatus, errorThrown) {
    showIndexError(textStatus +': '+ errorThrown +'\n'+ jqXHR);
  });
};

// Get the movements of users.
const getGuestMovements = function(cb) {
  $.get('/api/movements')
  .done(function(data) {
    if(data.error) {
      console.log(data.error);
      return cb(null);
    }
    cb(data.data);
  })
  .fail(function(jqXHR, textStatus, errorThrown) {
    showIndexError(textStatus +': '+ errorThrown +'\n'+ jqXHR);
  });
};

// Pretty-print users in HTML.
const beautifyUsers = function(users) {
  // Table declaration.
  var r = '<table class="table table-sm table-striped">';
  // Row declaration.
  r += '<tr>';
  // Table headers.
  r += '<th>Nombre</th>';
  r += '<th>Estado</th>';
  // Row closing.
  r += '</tr>';
  for(var i in users) {
    if(users.hasOwnProperty(i)) {
      var user = users[i];
      // Get user status.
      if(user.has_entered === 1) {
        user.status = '<i class="fa fa-check fa-green"></i> Dentro desde las '+ beautifyTime(user.last_entry);
      } else {
        if(user.last_exit) {
          user.status = '<i class="fa fa-sign-out fa-red"></i> Fuera desde las '+ beautifyTime(user.last_exit);
        } else {
          user.status = '<i class="fa fa-times fa-gray"></i> No ha entrado';
        }
      }
      // Row declaration.
      r += '<tr>';
      // Columns.
      r += '<td>'+ user.name +'<br><small><b>'+ user.code +'</b></small></td>';
      r += '<td>'+ user.status +'</td>';
      // Row closing.
      r += '</tr>';
    }
  }
  // Table closing.
  r += '</table>';
  return r;
};

// Pretty-print movements in HTML.
const beautifyMovements = function(movements) {
  // Table declaration.
  var r = '<table class="table table-sm table-striped">';
  // Row declaration.
  r += '<tr>';
  // Table headers.
  r += '<th>Nombre</th>';
  r += '<th>Evento</th>';
  r += '<th>Fecha</th>';
  r += '<th>Por</th>';
  // Row closing.
  r += '</tr>';
  for(var i in movements) {
    if(movements.hasOwnProperty(i)) {
      var movement = movements[i];
      // Get movement event.
      if(movement.type === 'entry') {
        movement.event = '<span class="label label-success">Entrada</span>';
      } else if(movement.type === 'exit') {
        movement.event = '<span class="label label-danger">Salida</span>';
      }
      // Row declaration.
      r += '<tr>';
      // Columns.
      r += '<td>'+ movement.name +'</td>';
      r += '<td>'+ movement.event +'</td>';
      r += '<td>'+ beautifyTime(movement.date) +'</td>';
      r += '<td class="registerer">'+ movement.registerer +'</td>';
      // Row closing.
      r += '</tr>';
    }
  }
  // Table closing.
  r += '</table>';
  return r;
};

// Function to stop mediastreamand hide QR scanner.
const stopScanner = function() {
  track.stop();
  $('#scanner').modal('hide');
};

// Function to be executed on QR code scan result.
const onScanSuccess = function(obj) {
  stopScanner();
  $('#code').val(obj.data);
};

$(document).ready(function() {
  $('#user-entry').click(function() {
    userEntry($('#code').val());
  });

  $('#user-exit').click(function() {
    userExit($('#code').val());
  });

  // $('#user-drink').click(function() {
  //   userDrink($('#code').val());
  // });

  $('button.close').click(function() {
    hideIndexError();
    hideIndexSuccess();
  });

  $('#show-movements').click(function() {
    $('#guest-movements .modal-body').html(loader);
    $('#guest-movements').modal('show');
    getGuestMovements(function(movements) {
      $('#guest-movements .modal-body').html(beautifyMovements(movements));
    });
  });

  $('#show-status').click(function() {
    $('#guest-status .modal-body').html(loader);
    $('#guest-status').modal('show');
    getGuestStatus(function(users) {
      $('#guest-status .modal-body').html(beautifyUsers(users));
    });
  });

  // Create video element inscanner modal.
  var canvasElement = document.getElementById("video-container");
  var canvas = canvasElement.getContext("2d");
  var video = document.createElement("video");

  // Function that is executed when the video element is scanning for a QR code.
  const tick = function() {
    if (video.readyState === video.HAVE_ENOUGH_DATA) {
      canvasElement.hidden = false;
      canvasElement.height = video.videoHeight;
      canvasElement.width = video.videoWidth;
      canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);
      var imageData = canvas.getImageData(0, 0, canvasElement.width, canvasElement.height);
      var code = jsQR(imageData.data, imageData.width, imageData.height, {
        inversionAttempts: "dontInvert",
      });
      if(code) {
        onScanSuccess(code);
        stopScanner();
        return;
      }
    }
    requestAnimationFrame(tick);
  };

  $('#scan').click(function() {
    // Use facingMode: environment to attempt to get the front camera on phones.
    navigator.mediaDevices.getUserMedia({video: {facingMode: "environment"}})
    .then(function(stream) {
      track = stream.getTracks()[0];
      video.srcObject = stream;
      video.setAttribute("playsinline", true);
      video.play();
      $('#scanner').modal('show');
      requestAnimationFrame(tick);
    });

    $('#scanner').on('hidden.bs.modal', function (e) {
      stopScanner();
    });
  });
});
