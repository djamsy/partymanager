// Show an error on login page.
const showLoginError = function(msg) {
  $('#login-error').html(msg);
  $('.form-signin .alert-danger').show();
};

// Hide error on login page.
const hideLoginError = function() {
  $('#login-error').html('');
  $('.form-signin .alert-danger').hide();
};

// Performs login by making POST to API.
const performLogin = function(username, password) {
  var redir = getUrlParam(window.location.href, 'redir') || '/';
  // Error control.
  if(!username.length) {
    return showLoginError('Please, type a valid username.');
  }
  if(!password.length) {
    return showLoginError('Please, type a valid password.');
  }
  // Perform POST.
  $.post('/api/login', {
    username: username,
    password: crypt(password)
  })
  .done(function(data) {
    if(data.error) {
      return showLoginError(data.error);
    }
    window.location.href = redir;
  })
  .fail(function(jqXHR, textStatus, errorThrown) {
    showLoginError(textStatus +': '+ errorThrown +'\n'+ jqXHR);
  });
};

// Perform logout by making POST to API
const performLogout = function(redir) {
  var redir = getUrlParam(window.location.href, 'redir') || '/';
	$.post('/api/logout')
	.done(function(data) {
		log.debug(data);
		window.location.href = redir;
	});
};

$(document).ready(function() {
  // Login.
  $('#login').click(function() {
    performLogin($('#username').val(), $('#password').val());
  });
  // Log out.
  $('#logout').click(performLogout);
  // When clikcing 'enter', login.
  // When pressing 'ENTER' key, preform login
  $(document).keyup(function (e) {
    if(($('#username').is(":focus") || $('#password').is(":focus")) && e.keyCode === 13) {
      performLogin($('#username').val(), $('#password').val());
    }
  });
});
