// Default loader used in application.
const loader = '<i class="fa fa-spinner fa-spin"></i>';

// Function for encrypting passwords.
const crypt = function(data) {
	return CryptoJS.SHA256(data).toString(CryptoJS.enc.Hex);
};

// Get query parameters.
const getUrlParams = function(url) {
	var parts = url.split('?');
	if(parts.length <2) {
		return [];
	}
	return parts[1].split('&').map(function(queryStr) {
		var param = queryStr.split('=');
		return {
			key: param[0],
			value: param[1] || null
		};
	});
};

// Return a query parameter.
const getUrlParam = function(url, param) {
	var params = getUrlParams(url);
	for(var i in params) {
		if(params[i].key === param) {
			return params[i].value;
		}
	}
	return null;
};

// Return true if an URL contains a query parameter.
const pathContainsParam = function(path, paramKey, paramValue) {
	var params = getUrlParams(path);
	return params.indexOf({key: paramKey, value: paramValue}) !== -1;
};

// Load a specific section.
const loadSection = function(section) {
	$('.section').hide();
	$('#'+ section).show();
};

// Force a number to be n digits.
const forceDigits = function(number, numDigits) {
	var numToStr = number + '';
	for(var i=numToStr.length; i<numDigits; i++) {
		numToStr = '0' + numToStr;
	}
	return numToStr;
};

// Beautify a JS date.
const beautifyTime = function(dateString) {
  var date = new Date(dateString);
  return ''+
		forceDigits(date.getHours(), 2) + ':' + /* Hour */
		forceDigits(date.getMinutes(), 2) + ':' + /* Minutes */
		forceDigits(date.getSeconds(), 2); /* Seconds */
};
