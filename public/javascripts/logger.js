// =============================================================================
// Constants.
// =============================================================================

// Log level (1: error, 2: info, 3: warn, 4: debug).
var logLevel = 3;

// =============================================================================
// Aux. functions.
// =============================================================================

// Returns everything but the last entry of the array. Source: underscore.js.
const us_initial = function(array, n, guard) {
	return Array.prototype.slice.call(array, 0, Math.max(0, array.length - (n == null || guard ? 1 : n)));
};

// Get the first element of an array. Source: underscore.js.
const us_first = function(array, n, guard) {
	if (array == null) return void 0;
	if (n == null || guard) return array[0];
	return us_initial(array, array.length - n);
};

// Returns everything but the first entry of the array. Source: underscore.js.
const us_rest = function(array, n, guard) {
	return Array.prototype.slice.call(array, n == null || guard ? 1 : n);
};

// Build list of items.
const buildUnorderedList = function(data) {
	var r = '';
	r+= '<ul>';
	for(var i in data) {
		r+= '<li>'+data[i]+'</li>';
	}
	r+= '</ul>';
	return r;
};

// Logging function.
const logging_fn = function(type, message) {
	console.log.apply(this, ['['+ type +'] '+ us_first(message)].concat(us_rest(message)));
};

// =============================================================================
// Objects.
// =============================================================================

// Global logger object.
const log =	{
	error: function() {
		if(logLevel >= 1) {
			return logging_fn('error', arguments);
		}
	},
	warn: function() {
		if(logLevel >= 2) {
			return logging_fn('warn', arguments);
		}
	},
	info: function() {
		if(logLevel >= 3) {
			return logging_fn('info', arguments);
		}
	},
	success: function() {
		// Same log level as 'info'.
		if(logLevel >= 3) {
			return logging_fn('success', arguments);
		}
	},
	query: function() {
		// Same log level as 'info'.
		if(logLevel >= 3) {
			return logging_fn('query', arguments);
		}
	},
	debug: function() {
		if(logLevel >= 4) {
			return logging_fn('debug', arguments);
		}
	}
};
