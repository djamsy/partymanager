// =============================================================================
// Dependencies.
// =============================================================================

// Node modules.
const path = require('path');

// Own modules.
const api = require(path.join(__dirname, '..', 'api'));
const fns = require(path.join(__dirname, '..', 'tools', 'functions'));
const errors = require(path.join(__dirname, '..', 'tools', 'errors'));

// =============================================================================
// Common API responses.
// =============================================================================

// Userinfo.
module.exports.info = function(req, res, next) {
    if(!req.user) {
        return res.api.error(errors.NO_USER);
    }
    res.api.ok(req.user);
};

// User roles requirements.
module.exports.require = {
    // Require a user.
    user: function(req, res, next) {
        if(!req.user) {
            return res.api.error(errors.NO_USER);
        }
        return next();
    },
    // Require a staff/host user.
    staff: function(req, res, next) {
        if(!req.user) {
            return res.api.error(errors.NO_USER);
        }
        if(!(req.user.role === 'host' || req.user.role === 'staff')) {
            return res.api.error(fns.formatError(errors.FORBIDDEN_ACCESS, 'staff/host'));
        }
        return next();
    }
};
