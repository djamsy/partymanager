// =============================================================================
// Dependencies.
// =============================================================================

// Node modules.
const glob = require('glob');
const path = require('path');

// Own modules.
const errors = require(path.join(__dirname, '..', 'tools', 'errors'));

// =============================================================================
// Functions.
// =============================================================================

// OK response.
var api_ok = function(res) {
  return function(data) {
    return res.json({
      code: 0,
      data: data
    });
  };
};
// Not OK response.
var api_err = function(res) {
  return function(err, code) {
    var e = err;
    if (err instanceof Error) {
      e = err.name + ':' + err.message;
    }
    if (err && err.code && err.message) {
      code = err.code;
      e = err.message;
    }
    return res.json({
      code: code || 1,
      error: e /* Translated */
    });
  };
};

// =============================================================================
// Module exports.
// =============================================================================

// Module exports.
module.exports = {
  // Initialization.
  init: function(req, res, next) {
    res.api = {};
    res.api.ok = api_ok(res);
    res.api.error = api_err(res);
    next();
  },
  // Error handler.
  error: function(err, req, res, next) {
    if (err) {
      log.error("Body: " + err.body);
      log.error("Stack: " + err.stack);
      return api_err(res)(errors.WRONG_SYNTAX);
    } else {
      next();
    }
  },
  // Default fallback.
  fallback: function(req, res) {
    log.debug('Unknown access point: "%s".', req.path);
    return api_err(res)(errors.UNKNOWN_AP);
  }
};

// =============================================================================
// Export other controllers.
// =============================================================================

// Export JS files in same folder as part of API.
var files = glob.sync(path.join(__dirname, '*.js'));
files.forEach(function(file) {
	var name = path.basename(file, '.js');
	if(name !== 'index') {
		module.exports[name] = require(file);
  }
});
