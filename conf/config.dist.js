module.exports = {
	// [OPTIONAL] Server port (number between 1 and 65535). Default is '3000'.
	port: 3000,
	// [OPTIONAL] Default language (ISO 639-1). Default is 'en'.
	lang: 'en',
	// [OPTIONAL] Domain name. Default is 'localhost'.
	domain: 'localhost',
	// [OPTIONAL] Logging level (1: ERROR, 2: WARN, 3: INFO, 4:DEBUG). Default is '1'.
	logLevel: 1,
	// [OPTIONAL] MySQL host. Default is 'localhost'.
	mysqlHost: 'localhost',
	// [REQUIRED] MySQL user.
	mysqlUser: '**********',
	// [REQUIRED] MySQL password.
	mysqlPass: '**********',
	// [REQUIRED] MySQL database.
	mysqlSchema: '**********',
	// [REQUIRED]: Party title.
	partyTitle: 'Custom title',
	// [REQUIRED]: Party date (YYYY-MM-DD HH:MM:SS).
	partyDate: '2018-09-21 18:00:00',
	// [REQUIRED]: Party description.
	partyDescription: 'Custom description'
};
