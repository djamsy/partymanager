// =============================================================================
// Module exports.
// =============================================================================

// Common queries to be used by several functions.
module.exports = {
  users: {
    getAll:          'SELECT * FROM users ORDER BY name ASC',
    getById:         'SELECT * FROM users WHERE id=?',
    getByUsername:   'SELECT * FROM users WHERE username LIKE ?',
    getByCode:       'SELECT u.*, r.name AS registerer FROM users u '+
                     'LEFT JOIN movements m ON m.id_user = u.id '+
                     'LEFT JOIN users r ON r.id = m.id_registerer '+
                     'WHERE u.code LIKE ? ORDER BY m.date DESC LIMIT 1',
    userEntry:       'UPDATE users SET has_entered=1, last_entry=? WHERE id=?',
    userExit:        'UPDATE users SET has_entered=0, last_exit=? WHERE id=?',
    updateLoginInfo: 'UPDATE users SET last_login=?, last_ip=? WHERE id=?',
    getWithoutCode:  'SELECT * FROM users WHERE code IS NULL OR code LIKE \'\'',
    updateCode:      'UPDATE users SET code=? WHERE id=?'
  },
  movements: {
    getAll: 'SELECT m.*, u.name, r.name AS registerer FROM movements m '+
            'INNER JOIN users u ON u.id = m.id_user '+
            'INNER JOIN users r ON r.id = m.id_registerer '+
            'ORDER BY date DESC',
    new:    'INSERT INTO movements(id_user, id_registerer, date, type) VALUES (?, ?, ?, ?)'
  }
};
