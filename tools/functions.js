// =============================================================================
// Dependencies.
// =============================================================================

// Node modules.
const _ = require('underscore');
const fs = require('fs');
const path = require('path');
const glob = require('glob');
const async = require('async');

// Own modules.
const errors = require(path.join(__dirname, 'errors'));
const datasets = require(path.join(__dirname, 'datasets'));
const dbDefinition = require(path.join(__dirname, '..', 'model', 'db'));

// =============================================================================
// Generic functions.
// =============================================================================

// Initialize fns object.
var fns = {};

// Checks that two objects have the same properties.
fns.sameProperties = function(obj, referenceObj) {
	var r = true;
	for(var i in referenceObj) {
		if(!obj[i]) {
			r = false;
		} else {
			if(typeof(referenceObj[i]) === 'object') {
				r = fns.sameProperties(obj[i], referenceObj[i]);
			}
		}
	}
	return r;
};

// Order values of an object from their integer keys and return an array.
fns.orderByKey = function(obj) {
	var orderedValues = [];
	var keys = Object.keys(obj).sort();
	for(var i in keys) {
		orderedValues.push(obj[keys[i]]);
	}
	return orderedValues;
};

// Replace characters '%p' with parameters.
fns.format = function(str, params, replaceChar) {
	replaceChar = replaceChar || '%p';
	if(!params) {
		return str;
	}
	if(typeof(params)!=='object') {
		params = [params];
	}
	var r = str;
	for(var i in params) {
		r = r.replace(replaceChar, params[i]);
	}
	return r;
};

// Convert a number to a string with the desired number of digits.
fns.forceDigits = function(number, numDigits) {
	var numToStr = number + '';
	for(var i=numToStr.length; i<numDigits; i++) {
		numToStr = '0' + numToStr;
	}
	return numToStr;
};

fns.beautifyTime = function(dateString) {
  var date = new Date(dateString);
  return ''+
		fns.forceDigits(date.getHours(), 2) + ':' + /* Hour */
		fns.forceDigits(date.getMinutes(), 2) + ':' + /* Minutes */
		fns.forceDigits(date.getSeconds(), 2); /* Seconds */
};

// Get the code of an error.
fns.getErrorCode = function(error) {
	return errors[error].code;
};

// Substitutes a character in an error object.
fns.formatError = function(error, replaceContent, replaceChar) {
	return {
		code: error.code,
		message: fns.format(error.message, replaceContent, replaceChar)
	};
};

// Pretty print a MySQL date (e.g. Nov. 12th 2012).
fns.beautifyDate = function(mysqlDate) {
	var date;
	if(!mysqlDate) {
		date = new Date();
	} else {
		date = new Date(mysqlDate);
	}
	return datasets.months[date.getMonth()+1].substr(0, 3) + '. ' /* month */ +
		datasets.cardinals[date.getDate()] + ' ' /* Day */ +
		date.getFullYear(); /* year */
};

// Pretty print a MySQL datetime (e.g. Nov. 2012 12 00:00:00).
fns.beautifyDateTime = function(mysqlDate) {
	var date;
	if(!mysqlDate) {
		date = new Date();
	} else {
		date = new Date(mysqlDate);
	}
	return datasets.months[date.getMonth()+1].substr(0, 3) + '. ' /* month */ +
		datasets.cardinals[date.getDate()] + ' ' /* Day */ +
		date.getFullYear() + ' ' /* year */ +
		fns.forceDigits(date.getHours(), 2) + ':' + /* Hour */
		fns.forceDigits(date.getMinutes(), 2) + ':' + /* Minutes */
		fns.forceDigits(date.getSeconds(), 2); /* Seconds */
};

// Pretty print a MySQL datetime (e.g. Nov. 2012 12 00:00:00).
fns.loggerDateTime = function(mysqlDate) {
	var date;
	if(!mysqlDate) {
		date = new Date();
	} else {
		date = new Date(mysqlDate);
	}
	return date.getFullYear() + '-' /* year */ +
		fns.forceDigits(date.getMonth() +1, 2) + '-' /* month */ +
		fns.forceDigits(date.getDate(), 2) + ' ' /* Day */ +
		fns.forceDigits(date.getHours(), 2) + ':' + /* Hour */
		fns.forceDigits(date.getMinutes(), 2) + ':' + /* Minutes */
		fns.forceDigits(date.getSeconds(), 2); /* Seconds */
};

// Returns a random integer between min (inclusive) and max (inclusive).
fns.getRandomInt = function(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
};

// Generate an alphanumeric code for a guest.
fns.generateCode = function(len) {
	len = len || config.guestCodeLength;
	var code = '';
	for(var i=0; i<len; i++) {
		var pos = fns.getRandomInt(0, datasets.codeCharset.length);
		code += datasets.codeCharset[pos];
	}
	return code;
};

// Validate a code of a guest.
fns.validateCode = function(code) {
	return (new RegExp('['+ datasets.codeCharset +']{'+ config.guestCodeLength +','+ config.guestCodeLength +'}')).test(code);
};

// Return route to a user's profile picture.
fns.getProfilePicture = function(user) {
	return path.join(config.userImageLocation, user.role +'.png');
};

// Replace variables in a string.
fns.replaceVars = function(str, vars) {
	var r = str;
	for(var key in vars) {
		if(vars.hasOwnProperty(key)) {
			var re = new RegExp('\\{\\{[ ]*'+ key +'[ ]*\\}\\}', 'g');
			r = r.replace(re, vars[key]);
		}
	}
	return r;
};

// Return logo.
fns.getLogoASCII = function() {
	// Generated with http://patorjk.com/software/taag.
  return "" +
  '  ▄▄▄· ▄▄▄· ▄▄▄  ▄▄▄▄▄ ▄· ▄▌    • ▌ ▄ ·.  ▄▄▄·  ▐ ▄  ▄▄▄·  ▄▄ • ▄▄▄ .▄▄▄  '+'\n'+
  ' ▐█ ▄█▐█ ▀█ ▀▄ █·•██  ▐█▪██▌    ·██ ▐███▪▐█ ▀█ •█▌▐█▐█ ▀█ ▐█ ▀ ▪▀▄.▀·▀▄ █·'+'\n'+
  '  ██▀·▄█▀▀█ ▐▀▀▄  ▐█.▪▐█▌▐█▪    ▐█ ▌▐▌▐█·▄█▀▀█ ▐█▐▐▌▄█▀▀█ ▄█ ▀█▄▐▀▀▪▄▐▀▀▄ '+'\n'+
  ' ▐█▪·•▐█ ▪▐▌▐█•█▌ ▐█▌· ▐█▀·.    ██ ██▌▐█▌▐█ ▪▐▌██▐█▌▐█ ▪▐▌▐█▄▪▐█▐█▄▄▌▐█•█▌'+'\n'+
  ' .▀    ▀  ▀ .▀  ▀ ▀▀▀   ▀ •     ▀▀  █▪▀▀▀ ▀  ▀ ▀▀ █▪ ▀  ▀ ·▀▀▀▀  ▀▀▀ .▀  ▀'+'\n';
};

// Export.
module.exports = fns;
