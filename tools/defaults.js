// =============================================================================
// Requirements.
// =============================================================================

// 	NPM requirements.
const fs = require('fs');
const path = require('path');

// Own modules.
const fns = require(path.join(__dirname, 'functions'));

// =============================================================================
// Aux functions.
// =============================================================================

// Validate a port number.
const isValidPort = function(port) {
	// Must be a number between 1 and 65535.
	return (!isNaN(port) && (1<=Number(port) && 65535>=Number(port)));
};

// Validate an IP or domain.
const isDomainOrIP = function(domain) {
	// Must be either 'localhost' or a valid IP/domain.
	var ipRegex = /(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}/;
	var domainRegex = /^[a-zA-Z0-9-.]+\.[a-zA-Z]{2,}$/;
	return (ipRegex.test(domain) || domainRegex.test(domain) || domain === 'localhost');
};

// Validate an e-mail.
const isValidEmail = function(email) {
	return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
};

// Validate a date with format YYYY-MM-DD HH:MM:SS.
const isValidDate = function(date) {
	return /[0-9]{4,4}-[0-9]{2,2}-[0-9]{2,2} [0-9]{2,2}:[0-9]{2,2}:[0-9]{2,2}/.test(date);
};

// =============================================================================
// Module exports.
// =============================================================================

// Configuration file definition.
module.exports = function(config) {
	return {
		// Server port.
		port: {
			required: false,
			type: 'number',
			default: 3000,
			validate: isValidPort,
			errMsg: 'must be a number between 1 and 65535'
		},
		// Domain.
		domain: {
			required: false,
			type: 'string',
			default: 'localhost',
			validate: isDomainOrIP,
			errMsg: 'must be a valid domain name or \'localhost\''
		},
		// Default language.
		lang: {
			required: false,
			type: 'string',
			default: 'en',
			validate: function(iso) {
				// Must be a ISO 639-1 code.
				return iso.length === 2;
			},
			errMsg: 'must be a valid ISO 639-1 code (two lowercase letters)'
		},
		// Logging level.
		logLevel: {
			required: false,
			type: 'number',
			default: 1,
			validate: function(logLevel) {
				// Must be a number between 0 and 4.
				return [1, 2, 3, 4].indexOf(logLevel) !== -1;
			},
			errMsg: 'must be a number between 1 and 4'
		},
		// MySQL host.
		mysqlHost: {
			required: false,
			type: 'string',
			default: 'localhost',
			validate: isDomainOrIP,
			errMsg: 'must be either "localhost" or a valid IP/domain'
		},
		// MySQL user.
		mysqlUser: {
			required: true,
			type:'string'
		},
		// MySQL password.
		mysqlPass: {
			required: true,
			type:'string'
		},
		// MySQL database.
		mysqlSchema: {
			required: true,
			type:'string'
		},
		// Party title.
		partyTitle: {
			required: true,
			type: 'string'
		},
		// Party description.
		partyDescription: {
			required: true,
			type: 'string'
		},
		// Party date.
		partyDate: {
			required: true,
			type: 'string',
			validate: isValidDate,
			errMsg: 'must be a valid date in format YYYY-MM-DD HH:MM:SS'
		}
	};
};
