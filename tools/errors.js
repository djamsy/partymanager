// =============================================================================
// Requirements.
// =============================================================================

// Node modules.
const _ = require('underscore');

// =============================================================================
// Define errors.
// =============================================================================

// API errors.
const apiErrors = {
	// API errors.
	WRONG_SYNTAX:               {code: 100, message: 'Wrong syntax'},
	UNKNOWN_AP:                 {code: 101, message: 'Unknown access point'},
	RESOURCE_NOT_FOUND:         {code: 102, message: 'Resource not found'},
	WRONG_REQ_PARAMS:           {code: 103, message: 'Wrong request parameters'},
	NO_RESULTS_FOUND:           {code: 104, message: 'No results found'},
	NO_PARAMS:                  {code: 105, message: 'Missing params'},
	// Validation errors.
	MISSING_FIELDS:             {code: 300, message: 'Missing field "%p"'},
	REQUIRED_FIELD:             {code: 301, message: 'Missing required field "%p"'},
	WRONG_TYPE:                 {code: 302, message: 'Wrong type "%p" for field "%p"'},
	WRONG_FIELDS:               {code: 303, message: 'Error in field "%p": %p'},
	// Auth errors.
	DENIED_ACCESS:              {code: 400, message: 'Access denied'},
	LOGIN_ERROR:                {code: 401, message: 'Login error'},
	INVALID_USER:               {code: 403, message: 'El usuario no existe.'},
	WRONG_PASSWORD:             {code: 404, message: 'Contraseña incorrecta.'},
	BLOCKED_USER:               {code: 405, message: 'Disabled user'},
	FORBIDDEN_ACCESS:           {code: 406, message: 'Role "%p" required'},
	NO_USER:                    {code: 407, message: 'No session found'},
	EARLY_LOGIN:                {code: 408, message: 'You can\'t login until %p'},
	LATE_LOGIN:                 {code: 409, message: 'You can\'t login since %p'},
	// Code errors.
	WRONG_CODE:                 {code: 500, message: 'El código "%p" no existe.'},
	ALREADY_ENTERED:            {code: 501, message: '%p (código: <b>%p</b>) ya ha entrado a las <b>%p</b> (registrado por %p).'},
	ALREADY_EXITED:             {code: 501, message: '%p (código: <b>%p</b>) ya ha salido a las <b>%p</b> (registrado por %p).'}
};

// Client-side errors.
const clientSideErrors = {
	404: {
		code: 404,
		status: 'Not Found',
		message: 'The requested URL doesn\'t exist or is no longer available',
		image: '/images/sad.ico'
	},
	500: {
		code: 500,
		status: 'Internal Server Error',
		message: 'There was an error while processing your request',
		image: '/images/sad.ico'
	},
	401: {
		code: 401,
		status: 'Unauthorized',
		message: 'You are not authorized to perform the requested action',
		image: '/images/sad.ico'
	}
};

// =============================================================================
// Module exports.
// =============================================================================

module.exports = _.extend(apiErrors, clientSideErrors);
