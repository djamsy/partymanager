// =============================================================================
// Dependencies.
// =============================================================================

// Node modules.
const path = require('path');

// Own modules.
const api = require(path.join(__dirname, '..', 'api'));
const controllers = require(path.join(__dirname, '..', 'controllers'));

// =============================================================================
// Route definition.
// =============================================================================

module.exports = function(app) {
	// Authentication functions.
	app.post('/api/login', controllers.auth.login, api.common.ok);
	app.post('/api/logout', controllers.auth.logout, api.common.ok);
	// User information.
	app.get('/api/user', api.auth.info);
	// User movements.
	app.get('/api/movements', api.auth.require.staff, controllers.users.movements, api.common.data);
	app.get('/api/users', api.auth.require.staff, controllers.users.list, api.common.data);
	app.post('/api/user/enter', api.auth.require.staff, controllers.users.enter, api.common.data);
	app.post('/api/user/exit', api.auth.require.staff, controllers.users.exit, api.common.data);
};
