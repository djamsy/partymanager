// =============================================================================
// Dependencies.
// =============================================================================

// Node modules.
const path = require('path');
const glob = require('glob');

// Own modules.
const api = require(path.join(__dirname, '..', 'api'));
const controllers = require(path.join(__dirname, '..', 'controllers'));

// =============================================================================
// Module exports.
// =============================================================================

module.exports = {
    // Function for initializing the router.
    mount: function(app, cb) {
        // Prepare the error handler.
        app.use('/api/*', api.error);
        // Init the res obj in api calls.
        app.all('/api/*', api.init);
        // Init the res obj in controllers.
        app.all('/*', controllers.init);

        return glob(path.join(__dirname, '*.js'), function(err, files) {
            for(var i in files) {
              if(files.hasOwnProperty(i)) {
                var filename = files[i];
                if (!/index.js$/.test(filename)) {
                  var router = require(filename);
                  router(app);
                }
              }
            }
            //  Default fallback.
            app.all('/api/*', api.fallback);
            app.all('/*', controllers.fallback);
            cb();
        });
    }
};
