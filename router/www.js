// =============================================================================
// Dependencies.
// =============================================================================

// Node modules.
const path = require('path');

// Own modules.
const controllers = require(path.join(__dirname, '..', 'controllers'));

// =============================================================================
// Route definition.
// =============================================================================

module.exports = function(app) {
	// Index.
	app.get('/', controllers.auth.require.user, controllers.render('index'));
	// Login.
	app.get('/login', controllers.render('users/login', 'layout'));
};
