// =============================================================================
// Dependencies.
// =============================================================================

// Node modules.
const bodyParser = require('body-parser');
const partials = require('express-partials');
const passport = require('passport');
const session = require('express-session');
const express = require('express');
const morgan = require('morgan');
const http = require('http');
const path = require('path');
const fs = require('fs');

// Own modules.
const router = require(path.join(__dirname, 'router', 'index'));
const modules = require(path.join(__dirname, 'modules', 'index'));

// =============================================================================
// Functions
// =============================================================================

// Event listener for HTTP server "error" event.
const onError = function(error, port) {
	if (error.syscall !== 'listen') {
		throw error;
	}

	var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

	// Handle specific listen errors with friendly messages.
	switch (error.code) {
		case 'EACCES':
			console.error(bind + ' requires elevated privileges');
			process.exit(1);
			break;
		case 'EADDRINUSE':
			console.error(bind + ' is already in use');
			process.exit(1);
			break;
		default:
			throw error;
	}
};

// Returns the response code escaped by a ANSII color.
const responseColor = function(req, res) {
    var status = res.statusCode;
		var color;

    switch(Math.floor(status/100)) {
    	case 5:  color = 31; break; /* 5XX: Red    */
    	case 4:  color = 33; break; /* 4XX: Yellow */
    	case 3:  color = 36; break; /* 3XX: Cyan   */
    	default: color = 32;        /* 2XX: Green  */
    }

    return '\x1b[' + color + 'm' + status + '\x1b[0m';
};

// Event listener for HTTP server "listening" event.
const onListening = function(server) {
	var addr = server.address();
	var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
	log.info('Listening on port %d.', addr.port);
};

// Build files/ directory and subdirectories if not present yet
const buildNotIncludedDirs = function(log) {
	const notIncludedFolders = [
		path.join(__dirname, 'logs'),
		path.join(__dirname, 'files'),
		path.join(__dirname, 'files', 'docs'),
		path.join(__dirname, 'files', 'docs', 'invitations'),
		path.join(__dirname, 'files', 'images'),
		path.join(__dirname, 'files', 'images', 'qr')
	];
	for(var i in notIncludedFolders) {
		if(!fs.existsSync(notIncludedFolders[i])) {
			log.info('Creating directory: %s', notIncludedFolders[i]);
			fs.mkdirSync(notIncludedFolders[i]);
		}
	}
};

// =============================================================================
// Create app.
// =============================================================================

// Init morgan.
morgan.token('status', responseColor);

// Create express app.
var app = express();

// Use morgan.
app.use(morgan('tiny'));

// view engine setup.
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// Declare public files URLs.
app.use(express.static(path.join(__dirname, 'public')));
app.use('/components', express.static(path.join(__dirname, 'node_modules')));

// Parse application/json and application/x-www-form-urlencoded.
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Partials.
app.use(partials());

// Express session.
app.use(session({
	secret: 'SabenAquellQueDiu',
	resave: true,
	saveUninitialized: false,
	cookie: {
		secure: false
	}
}));

// Init passport.
app.use(passport.initialize());
app.use(passport.session());

// Load modules.
modules.load(function(error) {

	if(error) {
		console.log(error);
		process.exit(1);
	}

	// Set app port after loading config.
	app.set('port', config.port);

	// Get number of routes.
	var nRoutes = app._router.stack.length;

	// Initialize router.
	router.mount(app, function() {

		// Print logo and intro message, defined in configuration.
		console.log(config.logo);
		console.log(config.introMsg);

		// Build directories that may not exist.
		buildNotIncludedDirs(log);

		// Get number of routes again.
		var nLoadedRoutes = app._router.stack.length - nRoutes;
		log.debug('%d routes loaded successfully.', nLoadedRoutes);

		// Create HTTP server.
		var server = http.createServer(app);

		// Listen on provided port, on all network interfaces.
		server.listen(config.port);
		server.on('error', onError, config.port);
		server.on('listening', function() { return onListening(server); });
	});
});
